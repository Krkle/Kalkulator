package com.example.ante.kalkulator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Zadatak extends Activity {

    TextView rezultat;
    EditText nb1, nb2;
    Button plus, minus, mnozenje, djeljenje;

    float rezultati;
    int br1, br2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadatak);
        rezultat = (TextView) findViewById(R.id.rezultat);
        nb1 = (EditText) findViewById(R.id.nb1);
        nb2 = (EditText) findViewById(R.id.nb2);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        mnozenje = (Button) findViewById(R.id.mnozenje);
        djeljenje = (Button) findViewById(R.id.djeljenje);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                br1 = Integer.parseInt(nb1.getText().toString());
                br2 = Integer.parseInt(nb2.getText().toString());
                rezultati = br1 + br2;
                rezultat.setText(String.valueOf(rezultati));
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                br1 = Integer.parseInt(nb1.getText().toString());
                br2 = Integer.parseInt(nb2.getText().toString());
                rezultati = br1 - br2;
                rezultat.setText(String.valueOf(rezultati));
            }
        });

        mnozenje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                br1 = Integer.parseInt(nb1.getText().toString());
                br2 = Integer.parseInt(nb2.getText().toString());
                rezultati = br1 * br2;
                rezultat.setText(String.valueOf(rezultati));
            }
        });

        djeljenje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                br1 = Integer.parseInt(nb1.getText().toString());
                br2 = Integer.parseInt(nb2.getText().toString());
                rezultati = br1 / br2;
                rezultati = br2 / br1;
                rezultat.setText(String.valueOf(rezultati));
            }
        });
    }
}
